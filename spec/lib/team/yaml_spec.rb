require 'spec_helper'
require_relative '../../../lib/team/yaml'

describe Gitlab::Homepage::Team::Yaml do
  let(:base_dir) do
    path = Pathname.new(__dir__) + '../../fixtures/data'
    path.expand_path
  end

  subject { described_class.new(base_dir) }

  it 'can split the team.yml file' do
    files = %w[
      officiis_nemo.yml
      omnis.possimus.yml
      qui_aut.yml
    ]

    files.each do |file|
      path = base_dir / 'team_members' / 'person' / file
      person = YAML.load_file(path)
      expect(subject).to receive(:to_file).with(path, person)
    end

    subject.split
  end

  it 'can join a directory of files' do
    fixture = Pathname.new(base_dir / 'team.yml')
    team = YAML.load_file(fixture)

    expect(subject).to receive(:to_file).with(fixture, match_array(team), described_class::HEADER)

    subject.join
  end

  describe '#verify!' do
    it 'does not raise' do
      expect { subject.verify! }.not_to raise_error
    end

    context 'the file are inconsistent' do
      let(:base_dir) do
        path = Pathname.new(__dir__) + '../../fixtures/inconsistent-team'
        path.expand_path
      end

      it 'raises an InconsistentTeamError' do
        expect { subject.verify! }
          .to raise_error(Gitlab::Homepage::Team::InconsistentTeamError)
      end
    end
  end
end
