---
layout: handbook-page-toc
title: "Application Security"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Application Security Mission

As part of the Security Engineering & Research sub-department, the application security team's mission is to support the business and ensure that all GitLab products securely manage customer data. We do this by working closely with both engineering and product teams.

## Application Security Roadmap

Please see the [Security Engineering and Research Program Strategy document][9].

## Roles & Responsibilities

Please see the [Application Security Job Family page][6].

## Application Security KPIs & Other Metrics in Sisense

* For KPIs and other metrics, please [see this page][7].
* For Embedded KPIs which you filter by section, stage, or group, please [see this page][8].

## General Role Functions

### Stable Counterparts

Please see the [Application Security Stable Counterparts page][4].

### Application Security Reviews

Please see the [Application Security Reviews page][1].

## Project ownership

Please see the [Appsec project owners page][2]

## Application Security Engineer Runbooks

Please see the [Application Security Engineer Runbooks page index][5]

## Meeting Recordings

The following recordings are available internally only:

* [AppSec Sync](https://drive.google.com/drive/folders/1sxnBhPNDofWg5JmKqrhEl5y4_aWldTbt)
* [AppSec Leadership Weekly](https://drive.google.com/drive/folders/1jyNYP2AOqoOPqr4qGMuh7PGha_j-7brb)

[1]: /handbook/engineering/security/security-engineering-and-research/application-security/appsec-reviews.html
[2]: /handbook/engineering/security/security-engineering-and-research/application-security/project-owners.html
[3]: /handbook/engineering/security/security-engineering-and-research/application-security/vulnerability-management.html
[4]: /handbook/engineering/security/security-engineering-and-research/application-security/stable-counterparts.html
[5]: /handbook/engineering/security/security-engineering-and-research/application-security/runbooks
[6]: /job-families/engineering/application-security/index.html
[7]: https://app.periscopedata.com/app/gitlab/641782/Appsec-hackerone-vulnerability-metrics
[8]: https://app.periscopedata.com/app/gitlab/758795/Appsec-Embedded-Dashboard
[9]: https://docs.google.com/document/d/1Mba9ZhuVr2qBkvR7AqzNTUFMUTapJqiXkPUqc9Gr8io/edit

## Backlog reviews

When necessary a backlog review can be initiated, please see the [Vulnerability Management Page][3] for more details.

## GitLab Secure Tools coverage

As part of our [dogfooding effort](/handbook/product/product-processes/#dogfood-everything), 
the [Secure Tools](https://docs.gitlab.com/ee/user/application_security/) are set up on the following GitLab projects:

| Project | SAST | Dependency Scanning | Container Scanning | DAST | Secrets Detection |
| ------ | ------ | ------ | ------ | ------ | ------ |
| [GitLab](https://gitlab.com/gitlab-org/gitlab) | ✅  | ✅  | `N/A`  | ✅<sup>1</sup>  |  |
| [Customers](https://gitlab.com/gitlab-org/customers-gitlab-com) | ✅  | ✅  | `N/A` | | | 
| [version](https://gitlab.com/gitlab-services/version-gitlab-com) | ✅ | ✅ | ✅ | ✅ | |
| [License](https://gitlab.com/gitlab-org/license-gitlab-com) | ✅ | ✅ | ✅ | ✅ | |
| [Gitaly](https://gitlab.com/gitlab-org/gitaly) | ✅<sup>2</sup> | ✅ | `N/A`<sup>11</sup> | `N/A` | |
| [Pages](https://gitlab.com/gitlab-org/gitlab-pages) | ✅  | ✅ | `N/A` | `N/A` | |
| [Workhorse](https://gitlab.com/gitlab-org/gitlab-workhorse) | ✅ | ✅ | `N/A` | `N/A` | |
| [Gitlab Shell](https://gitlab.com/gitlab-org/gitlab-shell) | ✅ | ✅ | `N/A` | `N/A` | |
| [Gitlab Runner](https://gitlab.com/gitlab-org/gitlab-runner) | ✅ | ✅ | | `N/A` | ✅ |
| [Gitlab Markup](https://gitlab.com/gitlab-org/gitlab-markup) | ✅<sup>8</sup> | `N/A`<sup>3</sup> | `N/A` | `N/A` | |
| [gitlab-ui](https://gitlab.com/gitlab-org/gitlab-ui/-/tree/master) | ✅ | ✅ | ✅<sup>9</sup> | `N/A`<sup>10</sup> |  | 
| [gitlab-exporter](https://gitlab.com/gitlab-org/gitlab-exporter) | ✅<sup>7</sup> | ✅<sup>6</sup> | ✅<sup>6</sup> | ✅<sup>6</sup> |
| [GitLab Omnibus](https://gitlab.com/gitlab-org/omnibus-gitlab) | | | | `N/A` | |
| [GitLab ElasticSearch Indexer](https://gitlab.com/gitlab-org/gitlab-elasticsearch-indexer) | ✅ | ✅ | `N/A` | `N/A` | |
| [release-cli](https://gitlab.com/gitlab-org/release-cli/) | ✅ | ✅ | ✅<sup>9</sup> | `N/A` | |
| [VS Code Extension](https://gitlab.com/gitlab-org/gitlab-vscode-extension) | ✅ | ✅ | `N/A` | `N/A` | |
| [figma plugin](https://gitlab.com/gitlab-org/gitlab-figma-plugin) | ✅ | ✅ | `N/A` | `N/A` | |
| [sketch plugin](https://gitlab.com/gitlab-org/gitlab-sketch-plugin) | ✅ | ✅ | `N/A` | `N/A` | |
| [GitLab Agent](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent) |  |  |  |  |  |
| [labkit](https://gitlab.com/gitlab-org/labkit) | ✅ |  |  |  |  |
| [labkit-ruby](https://gitlab.com/gitlab-org/labkit-ruby) |  |  |  |  |  |
| [gitlab-build-images](https://gitlab.com/gitlab-org/gitlab-build-images) | `N/A` | `N/A` |  | `N/A` |  |
| [gitlab-agent](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent) | ✅ | ✅ | ✅ | `N/A` | ✅ |
| [gitlab-terminal](https://gitlab.com/gitlab-org/gitlab-terminal) | ✅ | ✅ | `N/A` | `N/A` | ✅ |

1. DAST authenticated full scan configured and scheduled to [run nightly](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/38253). To-do: [https://gitlab.com/gitlab-com/gl-security/security-department-meta/-/issues/959#note_400990122](https://gitlab.com/gitlab-com/gl-security/security-department-meta/-/issues/959#note_400990122)
2. Only Go code is scanned. Gitaly's ruby code cannot be tested by brakeman (brakeman is specific to rails).
3. Dependency Scanning [checks also transitive dependencies](https://gitlab.com/gitlab-com/gl-security/engineering/issues/769#note_289559356) and , hence, DS does not need to be turned on for projects that are dependencies of other projects. It suffices to turn on DS on the parent project. For example, `gitlab-markup` is a dependency of `gitlab`. If DS is turned on for `gitlab`, `gitlab-markup` will also be checked for outdated deps.
4. [https://gitlab.com/gitlab-org/gitlab-runner/-/merge_requests/1897](https://gitlab.com/gitlab-org/gitlab-runner/-/merge_requests/1897)
5. TODO: Setup DAST scan to be authentified (see [https://gitlab.com/gitlab-com/gl-security/engineering/-/issues/769#note_315567580](https://gitlab.com/gitlab-com/gl-security/engineering/-/issues/769#note_315567580))
6. [https://gitlab.com/gitlab-org/gitlab-exporter/-/merge_requests/117](https://gitlab.com/gitlab-org/gitlab-exporter/-/merge_requests/117)
7. [https://gitlab.com/gitlab-org/gitlab-markup/-/merge_requests/26](https://gitlab.com/gitlab-org/gitlab-markup/-/merge_requests/26)
8. [https://gitlab.com/gitlab-org/release-cli/-/merge_requests/39](https://gitlab.com/gitlab-org/release-cli/-/merge_requests/39)
9. [https://gitlab.com/gitlab-org/gitlab-ui/-/merge_requests/1557](https://gitlab.com/gitlab-org/gitlab-ui/-/merge_requests/1557)
10. [https://gitlab.com/gitlab-com/gl-security/engineering/-/issues/769#note_379478139](https://gitlab.com/gitlab-com/gl-security/engineering/-/issues/769#note_379478139)
11. Container scanning on GitLab Omnibus will also perform checks for Gitaly [Setup standalone Gitaly](https://docs.gitlab.com/charts/advanced/external-gitaly/external-omnibus-gitaly.html#create-vm-with-omnibus-gitlab)


The results of these scans populate our Security Dashboards, which are [reviewed](/handbook/engineering/security/security-engineering-and-research/application-security/runbooks/security-dashboard-review.html) by our team.
