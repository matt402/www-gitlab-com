---
layout: markdown_page
title: "FY22-Q2 OKRs"
description: "View GitLabs Objective-Key Results for FY22 Q2. Learn more here!"
canonical_path: "/company/okrs/fy22-q2/"
---

This [fiscal quarter](/handbook/finance/#fiscal-year) will run from May 1, 2021 to July 31,2021.

## On this page
{:.no_toc}

- TOC
{:toc}

## OKR Schedule
The by-the-book schedule for the OKR timeline would be

| OKR schedule | Week of | Task |
| ------ | ------ | ------ |
|  |  |  |
