---
layout: markdown_page
canonical_path: "/topics/ci-cd/implement-continuous-integration/"
title: "Why you should implement continuous integration"
description: "Implementing continuous integration is a challenge. Find out the three essential tools you need and how to do continuous integration the right way."
---

[Continuous integration (CI)](/topics/ci-cd/) is the practice of merging code into a shared repository, usually several times a day. How continuous integrations works is that within the repository or production environment, automated building and testing is done to make sure there are no integration issues or problems with the code being merged. Continuous integration pipelines conduct automated tests. New code either passes these tests and proceeds to the next stage, or fails. This ensures that only CI-validated code ever makes it into production. 

![continuous integration workflow](/images/blogimages/cicd_pipeline_infograph.png)


## How to set up continuous integration

Continuous integration done well requires a mindset shift and a commitment to [DevOps best practices](/topics/devops/). In addition to organizational buy-in, there can be significant investments in infrastructure and tooling to consider. Technical leadership with DevOps knowledge and experience working in a cloud native environment will also be crucial for success.

## Essential continuous integration tools


Teams can invest in any combination of tools or cloud services, but teams implementing continuous integration for the first time should start with three essential tools:

*   A source code management (SCM) system such as [Git](/stages-devops-lifecycle/source-code-management/).
*   A shared source code repository that serves as a single source of truth for all code.
*   A continuous integration server that executes scripts for code changes that is integrated with your centralized source code repository (such as [GitLab CI](/stages-devops-lifecycle/continuous-integration/)).


## How to do continuous integration the right way

Continuous integration is more than just tools. While implementing CI tools is part of the process, there is a cultural shift that needs to happen as well. Continuous integration is one part of the larger [DevOps](/blog/2018/08/09/how-devops-and-gitlab-cicd-enhance-a-frontend-workflow/) mindset. To get the maximum benefits of continuous integration, keep in mind the tools and cultural needs:


### Commit code frequently

Continuous integration thrives with small, frequent changes to code. Code tested in small batches makes it easier for developers to identify bugs and errors and ensures better code quality.


### Avoid complexity in CI pipelines

It’s easy to introduce unnecessary complexity into development environments. Keep things as simple as possible and look for [boring solutions](/blog/2020/08/18/boring-solutions-faster-iteration/).


### Find the right continuous integration for your needs

Not all CI is created equal, so it’s important to [find the right CI](/topics/ci-cd/choose-continuous-integration-tool/) for your needs. Is it compatible with your cloud provider? Is it within budget? How does it compare to other similar tools? Does it have room for growth? Ask the right questions and you’ll find a CI solution that can help you in the long term.

## Learn more about continuous integration

[Making the case for CI/CD in your organization](/webcast/cicd-in-your-organization)

[GitLab vs. Jenkins comparison](/devops-tools/jenkins-vs-gitlab/)

[Need DevOps buy-in? Here's how to convince stakeholders](/blog/2020/09/24/devops-stakeholder-buyin/)
