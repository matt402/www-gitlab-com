# Team Member Database

This repository contains a database of GitLab team members, stored
in flat files. You will need to edit your file to keep it up-to-date,
in fact editing this database is one of your first tasks as a new
GitLab team member!

Currently the canonical version of this database is a single large
YAML file (`data/team.yml`). We are in the process of splitting this
up into individual files, one for each team member.

The `data/team.yml` file began, as many things do, as a simple solution
to the problem of keeping track of who we all are. Over time it became
enormous - a 26k line YAML file that everyone had to edit, breaking
tools and editors and sanity under its foot.

It is not strictly necessary (middleman can also access all these
separate files directly, under `data.team_members.person.sid` for 
example), and it may be removed in the future.

# Managing the database

There are some rake tasks that can be used to manage this database:

- `build:split_team`

  Takes the `date/team.yml` file and splits it into subfiles. This task
  is intended to be a temporary addition to allow us to move away from
  the monolithic team file. Each team member will be placed in a separate
  file in `data/team_members/{type}/{slug}.yml`. For example:
  `data/team_members/person/sid.yml`.

- `build:build_team`

  Takes the individual files in the `data/team_members` directory
  and builds a new `data/team.yml` file, that can be consumed by
  existing tools. Generating this artifact allows us to keep
  external tools working that rely on this single file.

- `build:verify_team`

  This loads the individual files on disk, and the combined file on disk
  and compares them. If they differ, the task aborts with an informative
  message.

